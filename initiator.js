const fetch = require('node-fetch');
const WebSocket = require('ws');
const Peer = require('simple-peer');
const wrtc = require('wrtc');
const SimplePeerJs = require('simple-peerjs')

async function main() {
  const peerId = process.argv[2];
  if (!peerId) {
    consoele.log('Please pass the peerId');
    process.exit(-1);
  }
  const connectionManager = new SimplePeerJs({
    fetch,
    WebSocket,
    wrtc,
    key: 'peerjs',
    host: 'dev.liberdus.com',
    port: 443,
    secure: true,
    path: '/peerserver/myapp'
  })
  const conn = await connectionManager.connect(peerId);

  console.log('Connected to peer!');
  conn.peer.send(`hey peer, I am a NODE peer, I generated a radom number => ${Math.random() * 1000}`);

  conn.peer.on('data', data => {
    console.log('Received data from peer ::', data.toString());
  });
}

main();