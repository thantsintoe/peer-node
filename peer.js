const fetch = require('node-fetch')
const WebSocket = require('ws')
const Peer = require('simple-peer')
const wrtc = require('wrtc')
const SimplePeerJs = require('simple-peerjs')

async function main () {
  const connectionManager = new SimplePeerJs({
    fetch,
    WebSocket,
    wrtc,
    key: 'peerjs',
    host: 'dev.liberdus.com',
    port: 443,
    secure: true,
    path: '/peerserver/myapp'
  })
  const peerId = await connectionManager.id
  console.log('My peer id:', peerId)

  connectionManager.on('connect', conn => {
    console.log('Peer connected:', conn.peerId)
    conn.peer.on('data', data => {
      console.log('Received data ::', data.toString())
      conn.peer.send('Fineee :)')
    })
  })
}

main()
